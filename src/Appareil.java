import java.util.*;
import java.util.List;
import java.awt.*;
import javax.*;


public interface Appareil {

	public void allumer();
	
	public void eteindre();
	
	public String toString();
	
}
