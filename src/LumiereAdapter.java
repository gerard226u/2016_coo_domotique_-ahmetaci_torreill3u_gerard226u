
public class LumiereAdapter implements Appareil{
	
	private Lumiere lum;
	
	public void allumer(){
	this.lum.changerIntensite(10);
	}
	
	public void eteindre(){
		this.lum.changerIntensite(0);
	}
	
}
