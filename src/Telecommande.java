import java.util.*;
import java.util.List;
import java.awt.*;
import javax.*;

public class Telecommande {
	
	public List<Appareil> apps;
	
	public Telecommande(){
		apps = new ArrayList<Appareil>();
	}
	
	public Telecommande(Lampe l){
		apps = new ArrayList<Appareil>();
		apps.add(l);
	}
	
	
	public void ajouterLampe(Lampe l){
		apps.add(l);
	}
	
	public void activerAppareil(int i){
		if(i<apps.size()){
			apps.get(i).allumer();
		}
	}
	
	public void desactiverAppareil(int i){
		apps.get(i).eteindre();
	}
	
	public void activerTout(){
		for(int i = 0; i<apps.size();  i++){
			apps.get(i).allumer();
		}
	}
	
	public int getNombre(){
		
		// ----------------------//
		int res = 0;
		return res;
	}
	
}
